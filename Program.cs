﻿namespace mkcs.AdvCertTool;

using System;
using System.Security.Cryptography.X509Certificates;

public class App
{
    public static int Main(string[] args)
    {
        if (args.Length == 0) {
            Console.WriteLine("Syntax: advcerttool filename");
            return -1;
        }

        var filename = args[0];
        Console.WriteLine($"Processing {filename}");
        try
        {
            using StreamReader streamReader = new(filename);
            // Read certificate from file and get rid off its header and footer
            var certificateRaw = streamReader.ReadToEnd()
                .Replace("-----BEGIN CERTIFICATE-----\n", string.Empty)
                .Replace("\n-----END CERTIFICATE-----", string.Empty);
            var certificateDecoded = Convert.FromBase64String(certificateRaw);
            X509Certificate2 cert = new(certificateDecoded);
            // Get the expiration date and calculate remaining valid days
            var expiration = DateTime.Parse(cert.GetExpirationDateString());
            var remainingDays = Math.Round((expiration - DateTime.Today).TotalDays, 0);
            // Print message
            if (remainingDays > 0)
            {
                Console.WriteLine($"Certificate is due to renew in {remainingDays} day{(remainingDays == 1 ? string.Empty : "s")}");
                return remainingDays > 7 ? 0 : 1;
            }
            else
            {
                Console.WriteLine("Certificate expired!");
                return 2;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
            return -2;
        }
    }
}